package co.dev.sqdm.hexeditor;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

import co.dev.sqdm.hexeditor.Modelo.DTO.InformacionArchivo;
import co.dev.sqdm.hexeditor.Modelo.DTO.InformacionBinaria;
import co.dev.sqdm.hexeditor.Modelo.OperacionesArchivo;

public class VentanaPrincipal extends AppCompatActivity {

    private Button btn_cargar;
    private ToggleButton btn_endian;
    private EditText txt_hexa,txt_char,txt_offset;
    private TextView txt_8bits,txt_8bits_signo,txt_16bits,txt_32bits,txt_64bits,txt_32bits_float,txt_64bits_float,txt_title;
    private TextView txt_nombre;
    private TextView txt_ruta;
    private TextView txt_tamanio;
    private TextView txt_fecha;
    private int READ_REQUEST_CODE =23;
    String tag = VentanaPrincipal.class.getName();
    Boolean scroll_sate=false;
    ScrollView scrollView;

    long offsetCounter = 1;
    int scroll_hito = 1000;
    int paso = 1600;
    Uri uri = null;

    private OperacionesArchivo operacionesArchivo = new OperacionesArchivo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_cargar = findViewById(R.id.btn_cargar);
        txt_hexa = findViewById(R.id.txt_hexa);
        txt_char = findViewById(R.id.txt_char);
        txt_8bits = findViewById(R.id.txt_8bits);
        txt_8bits_signo = findViewById(R.id.txt_8bits_signo);
        txt_16bits = findViewById(R.id.txt_16bits);
        txt_32bits = findViewById(R.id.txt_32bits);
        txt_64bits = findViewById(R.id.txt_64bits);
        txt_32bits_float = findViewById(R.id.txt_32bits_float);
        txt_64bits_float = findViewById(R.id.txt_64bits_float);
        btn_endian = findViewById(R.id.btn_endian);

        txt_offset = findViewById(R.id.txt_offset);
        scrollView = findViewById(R.id.scrollView);

        txt_nombre = findViewById(R.id.txt_nombre_archivo);
        txt_tamanio = findViewById(R.id.txt_tamanio_archivo);
        txt_ruta = findViewById(R.id.txt_ruta_archivo);
        txt_fecha = findViewById(R.id.txt_fecha_archivo);
        txt_title = findViewById(R.id.txt_title);

        btn_endian.setEnabled(false);
        txt_offset.setEnabled(false);
        txt_hexa.setEnabled(false);
        txt_char.setEnabled(false);


        scrollView.fullScroll(View.FOCUS_DOWN);

        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                /*Si se llega al final del hito convierta el archivo entre el rango determinado e incremente el hito*/
                if (i1>scroll_hito){
                    Log.i("Y: ",""+i1);
                    try {
                        txt_char.clearFocus();
                        InformacionArchivo infoArchivo = operacionesArchivo.obtenerInfoArchivo(uri,paso,paso+1500,VentanaPrincipal.this);
                        mostrarDatosArchivo(infoArchivo);
                        scroll_hito = scroll_hito + 1000;
                        paso = paso + 1600;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                scroll_sate=true;
            }
        });

        txt_hexa.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {
                    btn_endian.setEnabled(true);

                    String enteredText = txt_hexa.getText().toString();
                    int cursorPosition = txt_hexa.getSelectionStart();
                    txt_char.requestFocusFromTouch();
                    InformacionBinaria informacionBinaria = operacionesArchivo.tomarHex(enteredText,cursorPosition,btn_endian.isChecked());
                    mostrarConversionBits(informacionBinaria);
                    scroll_sate=false;
                }
                else{
                    Log.i("","not");
                }
            }
        });

        txt_hexa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!scroll_sate){

                    btn_endian.setEnabled(true);

                    String enteredText = txt_hexa.getText().toString();
                    int cursorPosition = txt_hexa.getSelectionStart();
                    txt_char.requestFocusFromTouch();
                    InformacionBinaria informacionBinaria = operacionesArchivo.tomarHex(enteredText,cursorPosition,btn_endian.isChecked());
                    mostrarConversionBits(informacionBinaria);
                }
                scroll_sate=false;
            }
        });

        btn_cargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                txt_offset.setEnabled(true);
                txt_hexa.setEnabled(true);
                txt_char.setEnabled(true);

                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, READ_REQUEST_CODE);
            }
        });
        btn_endian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InformacionBinaria informacionBinaria = operacionesArchivo.convertions(btn_endian.isChecked());
                mostrarConversionBits(informacionBinaria);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            if (resultData != null) {
                uri = resultData.getData();
                Log.i(tag, "Uri: " + uri.toString());
                try {
                    operacionesArchivo = new OperacionesArchivo();
                    limpiarCampos();
                    /*Abrir el archivo y convertirlo desde cero hasta un valor determinado*/
                    InformacionArchivo infoArchivo = operacionesArchivo.obtenerInfoArchivo(uri,0,paso,this);
                    mostrarDatosArchivo(infoArchivo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void mostrarDatosArchivo(InformacionArchivo infoArchivo){
        txt_nombre.setText(infoArchivo.getNombreArchivo());
        txt_ruta.setText(infoArchivo.getRuta());
        txt_tamanio.setText(infoArchivo.getTamanio());
        txt_fecha.setText(infoArchivo.getFecha());
        txt_char.append(infoArchivo.getContenidoArchivo().getViewContento());
        txt_hexa.append(infoArchivo.getContenidoArchivo().getHexContent());
        txt_offset.append(infoArchivo.getContenidoArchivo().getOffset());
    }
    private void mostrarConversionBits(InformacionBinaria informacionBinaria){
        if (informacionBinaria.getDato8BitSinSigno()!=null){
            Toast.makeText(VentanaPrincipal.this,informacionBinaria.getByteSeleccionado(),Toast.LENGTH_SHORT).show();
            txt_title.setText("Hex Editor "+informacionBinaria.getByteSeleccionado());
            txt_char.setSelection(informacionBinaria.getPosicion_ascii()-1,informacionBinaria.getPosicion_ascii());
            txt_8bits.setText(informacionBinaria.getDato8BitSinSigno());
            txt_8bits_signo.setText(informacionBinaria.getDato8BitConSigno());
            txt_16bits.setText(informacionBinaria.getDato16Bit());
            txt_32bits.setText(informacionBinaria.getDato32Bit());
            txt_64bits.setText(informacionBinaria.getDato64Bit());
            txt_32bits_float.setText(informacionBinaria.getDato32BitFloat());
            txt_64bits_float.setText(informacionBinaria.getDato64BitFloat());
        }

    }
    /*Metodo para limpiar los campos del Layout*/
    private void limpiarCampos(){
        txt_offset.setText("0");
        txt_char.setText(" ");
        txt_hexa.setText(" ");
        txt_title.setText("Hex Editor");
        txt_8bits.setText("8 bits sin signo: ");
        txt_8bits_signo.setText("8 bits con signo: ");
        txt_16bits.setText("16 bits: ");
        txt_32bits.setText("32 bits: ");
        txt_64bits.setText("64 bits: ");
        txt_32bits_float.setText("Float 32 bits: ");
        txt_64bits_float.setText("Float 64 bits: ");
        offsetCounter = 1;
        scroll_hito = 1000;
        paso = 1600;
    }

}
