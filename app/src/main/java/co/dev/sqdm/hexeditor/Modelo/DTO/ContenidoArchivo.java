package co.dev.sqdm.hexeditor.Modelo.DTO;

public class ContenidoArchivo
{
    private StringBuilder offset;
    private String hexContent;
    private String viewContent;

    public StringBuilder getOffset() {return offset;}
    public void setoffset(StringBuilder off_set) {offset = off_set;}

    public String getHexContent() {return hexContent;}
    public void sethexContent(String hex_Content) {hexContent = hex_Content;}

    public String getViewContento() {return viewContent;}
    public void setviewContent(String view_Content) {viewContent = view_Content;}
}
