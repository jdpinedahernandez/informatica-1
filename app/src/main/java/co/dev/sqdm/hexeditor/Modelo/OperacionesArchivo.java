package co.dev.sqdm.hexeditor.Modelo;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.StringBufferInputStream;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

import javax.xml.parsers.SAXParser;

import co.dev.sqdm.hexeditor.Modelo.DTO.ContenidoArchivo;
import co.dev.sqdm.hexeditor.Modelo.DTO.InformacionArchivo;
import co.dev.sqdm.hexeditor.Modelo.DTO.InformacionBinaria;

public class OperacionesArchivo
{
    ArrayList<String> list_8_Bytes;
    InformacionBinaria informacionBinaria;
    /*Metodo desde donde se extrae el contenido de un archivo*/
    public ContenidoArchivo extraerContenido(InputStream is, long inicio, long fin) throws IOException {
        FileInputStream fileInputStream = (FileInputStream) is;
        ByteBuffer byteBuffer;
        FileChannel channel;
        channel = fileInputStream.getChannel();

        if(fin > channel.size())
        {
            fin = channel.size();
        }
        byteBuffer = channel.map(
                FileChannel.MapMode.READ_ONLY,
                inicio,
                fin);
        ContenidoArchivo respuesta = new ContenidoArchivo();

        long offsetCounter = inicio;
        StringBuilder stringBuilder = new StringBuilder();
        int bytesCounter = 0;

        StringBuilder sbHex = new StringBuilder();
        StringBuilder sbResult = new StringBuilder();
        StringBuilder sbHexOffset = new StringBuilder();

        /*Recorrer byte a byte el arreglo bytes entre inicio y fin determinado*/
        for (int i = Math.toIntExact(inicio); i<fin ; i++) {

            try
            {

                /*Si el byte leido esta entre 33 y 126 concatene el contenido y sino concatene un caracter generico "." para mostrar el codigo ASCII*/
                if (byteBuffer.get(i) >= 33 && byteBuffer.get(i) <= 126) {
                    stringBuilder.append((char)byteBuffer.get(i));
                }
                else {
                    /*Caracteres no reconicidos*/
                    //char test = (char)bytes[i];
                    stringBuilder.append(".");
                }
                /*Conversión a hexa y concatenacion*/
                sbHex.append(String.format(" %02X", byteBuffer.get(i)));
                offsetCounter++;

                if (bytesCounter == 15) {
                    stringBuilder.append("\n");
                    sbResult.append(sbHex).append("\n");
                    sbHex.setLength(0);
                    bytesCounter = 0;
                    sbHexOffset.append("\n");
                    sbHexOffset.append(offsetCounter);

                } else {
                    bytesCounter++;
                }
            }
            catch (Exception e)
            {
                stringBuilder.append(".");
                break;
            }
        }

        respuesta.setoffset(sbHexOffset);
        respuesta.sethexContent(sbResult.toString());
        respuesta.setviewContent(stringBuilder.toString());

        return respuesta;
    }
    /*Metodo que abre el archivo y en donde se extrae el contenido del archivo*/
    public InformacionArchivo obtenerInfoArchivo(Uri uri , long inicio, long fin, Context context) throws IOException
    {
        InformacionArchivo infoArchivo = new InformacionArchivo();
        InputStream is  = context.getContentResolver().openInputStream(uri);
        /*File f = new File(uri.getPath());
        Date fechaModificacion = new Date(f.lastModified());*/

        FileInputStream fileInputStream = (FileInputStream) is;
        ByteBuffer byteBuffer;
        FileChannel channel;
        channel = fileInputStream.getChannel();
        long size = channel.size();

        /*SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = formatoFecha.format(fechaModificacion);*/
        infoArchivo = caracteristicasArchivo(obtenerNombreArchivo(uri,context), size, uri.getPath(), "");
        ContenidoArchivo cont = extraerContenido(is, inicio, fin);
        infoArchivo.setContenidoArchivo(cont);
        is.close();
        return infoArchivo;
    }

    public InformacionArchivo caracteristicasArchivo(String nombreArchivo, long tamanioBites, String ruta, String fechaModificacion)
    {
        InformacionArchivo info = new InformacionArchivo();

        info.setNombreArchivo("Nombre: " + nombreArchivo);
        info.setTamanio("Tamaño: " + tamanioBites + " Bites");
        info.setRuta("Ruta: " + ruta);
        info.setFecha("Fecha: " + fechaModificacion);

        return info;

    }
    private String obtenerNombreArchivo(Uri uriEntrada,Context context) {
        String result = null;
        if (uriEntrada.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uriEntrada, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uriEntrada.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
    /*Metodo desde donde se calcula la posición del cursor para la visualización de la información ascii*/
    public InformacionBinaria tomarHex(String enteredText,int cursorPosition,Boolean littleEndian){
        /*Si no se está haciendo srcroll en la vista*/
        informacionBinaria = new InformacionBinaria();
            try {
                list_8_Bytes=new ArrayList<>();
                /*Tomar el string que hay desde el inicio hasta la posicion del cursor seleccionada*/
                String enteredTextCursor=enteredText.substring(0,cursorPosition);

                /*tamaño del texto - cantidad de ocurrencias de \n en el texto*/
                int occurences = enteredTextCursor.length() - enteredTextCursor.replace("\n", "").length();
                /*Quitar la catidad de ocurrencias de \n a la posicion en que me encuentro para compararla con la cadena de texto sin \n*/
                cursorPosition = cursorPosition-occurences;
                enteredText = enteredText.replaceAll("\n","");

                list_8_Bytes=new ArrayList<>();
                /*representa la posicion del caracter en el string ASCII*/
                int position = enteredTextCursor.length() - enteredTextCursor.replace(" ", "").length()+occurences;
                /*Sombree el caracter correspondiente en ASCII*/
                informacionBinaria.setPosicion_ascii(position);
                /*Tomar el texto entre el caracter anterior y siguiente para saber si estoy antes, entre o despues del hexa*/
                /* .AD */
                if (enteredText.substring(cursorPosition-1,cursorPosition).equalsIgnoreCase(" ")
                        && !enteredText.substring(cursorPosition,cursorPosition+1).equalsIgnoreCase(" "))
                {
                    /*Capturar cada hexa e incluirlo en la lista list_8_Bytes*/
                    list_8_Bytes.add(enteredText.substring(cursorPosition,cursorPosition+2));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+3,cursorPosition+5));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+6,cursorPosition+8));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+9,cursorPosition+11));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+12,cursorPosition+14));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+15,cursorPosition+17));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+18,cursorPosition+20));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+21,cursorPosition+23));
                    informacionBinaria.setByteSeleccionado(enteredText.substring(cursorPosition,cursorPosition+2));
//                    Toast.makeText(VentanaPrincipal.this,enteredText.substring(cursorPosition,cursorPosition+2),Toast.LENGTH_SHORT).show();
                }/* A.D */
                else if (!enteredText.substring(cursorPosition-1,cursorPosition).equalsIgnoreCase(" ")
                        && !enteredText.substring(cursorPosition,cursorPosition+1).equalsIgnoreCase(" "))
                {
                    list_8_Bytes.add(enteredText.substring(cursorPosition-1,cursorPosition+1));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+2,cursorPosition+4));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+5,cursorPosition+7));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+8,cursorPosition+10));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+11,cursorPosition+13));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+14,cursorPosition+16));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+17,cursorPosition+19));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+20,cursorPosition+22));
                    informacionBinaria.setByteSeleccionado(enteredText.substring(cursorPosition-1,cursorPosition+1));

//                    txt_title.setText("Hex Editor "+enteredText.substring(cursorPosition-1,cursorPosition+1));
//                    Toast.makeText(VentanaPrincipal.this,enteredText.substring(cursorPosition-1,cursorPosition+1),Toast.LENGTH_SHORT).show();
                }/* AD. */
                else if (!enteredText.substring(cursorPosition-1,cursorPosition).equalsIgnoreCase(" ")
                        && enteredText.substring(cursorPosition,cursorPosition+1).equalsIgnoreCase(" "))
                {
                    list_8_Bytes.add(enteredText.substring(cursorPosition-2,cursorPosition));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+1,cursorPosition+3));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+4,cursorPosition+6));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+7,cursorPosition+9));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+10,cursorPosition+12));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+13,cursorPosition+15));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+16,cursorPosition+18));
                    list_8_Bytes.add(enteredText.substring(cursorPosition+19,cursorPosition+21));
                    informacionBinaria.setByteSeleccionado(enteredText.substring(cursorPosition-2,cursorPosition));

//                    txt_title.setText("Hex Editor "+enteredText.substring(cursorPosition-1,cursorPosition+1));
//                    Toast.makeText(VentanaPrincipal.this,enteredText.substring(cursorPosition-2,cursorPosition),Toast.LENGTH_SHORT).show();
                }
                informacionBinaria = convertions(littleEndian);
            } catch (Exception e) {
                e.printStackTrace();
            }

        return informacionBinaria;
    }
    public InformacionBinaria convertions(Boolean littleEndian){
        int bits_8 = 0;
        int bits_8_signed = 0;
        Float bits_32_float = (float) 0;
        Double bits_64_double = 0.0;
        int bits_16 = 0;
        BigInteger bits_32 = BigInteger.valueOf(0);
        BigInteger bits_64 = BigInteger.valueOf(0);
        if (!list_8_Bytes.isEmpty()){

            if (!littleEndian) //Big Endian
            {
                /*Integer max 4 bytes - 32 bits
                 * Long max 8 bytes 64 bits
                 * BigInteger (2 ^ 32) ^ Integer.MAX_VALUE*/
                String bits_32_str = list_8_Bytes.get(0) + list_8_Bytes.get(1) +
                        list_8_Bytes.get(2) + list_8_Bytes.get(3);
                String bits_64_str = list_8_Bytes.get(0) + list_8_Bytes.get(1) +
                        list_8_Bytes.get(2) + list_8_Bytes.get(3) +
                        list_8_Bytes.get(4) + list_8_Bytes.get(5) +
                        list_8_Bytes.get(6) + list_8_Bytes.get(7);
                /*conversion a base 16 (decimal) del numero hexa introducido*/
                bits_8 = Integer.parseInt(list_8_Bytes.get(0), 16);
                bits_8_signed = (byte) Integer.parseInt(list_8_Bytes.get(0), 16);
                bits_32_float = Float.intBitsToFloat(Long.valueOf(bits_32_str, 16).intValue());
                bits_64_double = Double.longBitsToDouble(new BigInteger(bits_64_str, 16).longValue());
                bits_16 = Integer.parseInt((list_8_Bytes.get(0) + list_8_Bytes.get(1)).replace(" ", ""), 16);
                bits_32 = new BigInteger((bits_32_str).replace(" ", ""), 16);
                bits_64 = new BigInteger((bits_64_str).replace(" ", ""), 16);

            }
            else {//Little Endian
                String bits_32_str = list_8_Bytes.get(3) + list_8_Bytes.get(2) +
                        list_8_Bytes.get(1) + list_8_Bytes.get(0);
                String bits_64_str = list_8_Bytes.get(7) + list_8_Bytes.get(6) +
                        list_8_Bytes.get(5) + list_8_Bytes.get(4) +
                        list_8_Bytes.get(3) + list_8_Bytes.get(2) +
                        list_8_Bytes.get(1) + list_8_Bytes.get(0);
                bits_8 = Integer.parseInt(list_8_Bytes.get(0), 16);
                bits_8_signed = (byte) Integer.parseInt(list_8_Bytes.get(0), 16);
                bits_32_float = Float.intBitsToFloat(Long.valueOf(bits_32_str, 16).intValue());
                bits_64_double = Double.longBitsToDouble(new BigInteger(bits_64_str, 16).longValue());
                bits_16 = Integer.parseInt((list_8_Bytes.get(1) + list_8_Bytes.get(0)).replace(" ", ""), 16);
                bits_32 = new BigInteger((bits_32_str).replace(" ", ""), 16);
                bits_64 = new BigInteger((bits_64_str).replace(" ", ""), 16);

            }
            informacionBinaria.setDato8BitSinSigno("8 bits sin signo: "+String.valueOf(bits_8));
            informacionBinaria.setDato8BitConSigno("8 bits con signo: "+String.valueOf(bits_8_signed));
            informacionBinaria.setDato16Bit("16 bits: "+String.valueOf(bits_16));
            informacionBinaria.setDato32Bit("32 bits: "+String.valueOf(bits_32));
            informacionBinaria.setDato64Bit("64 bits: "+String.valueOf(bits_64));
            informacionBinaria.setDato32BitFloat("Float 32 bits: "+String.valueOf(bits_32_float));
            informacionBinaria.setDato64BitFloat("Float 64 bits: "+String.valueOf(bits_64_double));
        }
        return informacionBinaria;
    }
}
