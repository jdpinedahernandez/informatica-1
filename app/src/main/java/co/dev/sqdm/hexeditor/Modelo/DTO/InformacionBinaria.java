package co.dev.sqdm.hexeditor.Modelo.DTO;

/**
 * Objeto de transferencia de datos los referentes a la información binaria del archivo
 */

public class InformacionBinaria
{
    private String dato8BitSinSigno;
    private String dato8BitConSigno;
    private String dato16Bit;
    private String dato32Bit;
    private String dato64Bit;
    private String dato32BitFloat;
    private String dato64BitFloat;
    private boolean esLittle;
    private String byteSeleccionado;
    private int posicion_ascii;

    public String getDato8BitSinSigno() {return dato8BitSinSigno;}
    public void setDato8BitSinSigno(String OchoBitSinSigno) {dato8BitSinSigno = OchoBitSinSigno;}

    public String getDato8BitConSigno() {return dato8BitConSigno;}
    public void setDato8BitConSigno(String OchoBitConSigno) {dato8BitConSigno = OchoBitConSigno;}

    public String getDato16Bit() {return dato16Bit;}
    public void setDato16Bit(String DieciseisBit) {dato16Bit = DieciseisBit;}

    public String getDato32Bit() {return dato32Bit;}
    public void setDato32Bit(String TreintaydosBit) {dato32Bit = TreintaydosBit;}

    public String getDato64Bit() {return dato64Bit;}
    public void setDato64Bit(String SesentaycuatroBit) {dato64Bit = SesentaycuatroBit;}

    public String getDato32BitFloat() {return dato32BitFloat;}
    public void setDato32BitFloat(String TreinteydosBitFloat) {dato32BitFloat = TreinteydosBitFloat;}

    public String getDato64BitFloat() {return dato64BitFloat;}
    public void setDato64BitFloat(String SesentaycuatroBitFloat) {dato64BitFloat = SesentaycuatroBitFloat;}

    public boolean getEsLittle() {return esLittle;}
    public void setEsLittle(boolean little) {esLittle = little;}

    public String getByteSeleccionado() {
        return byteSeleccionado;
    }

    public void setByteSeleccionado(String byteSeleccionado) {
        this.byteSeleccionado = byteSeleccionado;
    }
    public int getPosicion_ascii() {
        return posicion_ascii;
    }

    public void setPosicion_ascii(int posicion_ascii) {
        this.posicion_ascii = posicion_ascii;
    }
}
