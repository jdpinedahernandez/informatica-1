package co.dev.sqdm.hexeditor.Modelo.DTO;

import java.io.Serializable;
import java.util.Date;

/**
 * Objeto de transferencia de datos los referentes a las caracteristicas del archivo
 */

public class InformacionArchivo
{
    private String nombreArchivo;
    private String rutaArchivo;
    private String tamanioArchivo;
    private String fechaArchivo;
    private ContenidoArchivo contenidoArchivo;

    public String getNombreArchivo() {return nombreArchivo;}
    public void setNombreArchivo(String nombre) {nombreArchivo = nombre;}

    public String getRuta() {return rutaArchivo;}
    public void setRuta(String ruta) {rutaArchivo = ruta;}

    public String getTamanio() {return tamanioArchivo;}
    public void setTamanio(String tamanio) {tamanioArchivo = tamanio;}

    public String getFecha() {return fechaArchivo;}
    public void setFecha(String fecha) {fechaArchivo = fecha;}

    public ContenidoArchivo getContenidoArchivo() {
        return contenidoArchivo;
    }

    public void setContenidoArchivo(ContenidoArchivo contenidoArchivo) {
        this.contenidoArchivo = contenidoArchivo;
    }

}
